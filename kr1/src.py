import csv
import os
import sys
from fractions import Fraction

import numpy as np

criteria_file = sys.argv[1]
alternatives_dir = sys.argv[2]
alternatives_files = os.listdir(alternatives_dir)

criteria_names = []
criteria_cofs_table = None

with open(criteria_file) as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=",")
    row_number = 0
    for row_index, row in enumerate(csv_reader):
        if row_index == 0:
            for crit in row:
                if crit:
                    criteria_names.append(crit)
            criteria_cofs_table = np.empty([len(criteria_names), len(criteria_names)], dtype=float)
        else:
            for i in range(len(criteria_names)):
                criteria_cofs_table[row_index - 1, i] = float(Fraction(row[i + 1]))

is_inited = False
num_alternatives = 0
alternatives_cofs_tables_list = None

alternatives_names = []

for file in alternatives_files:
    f = open(alternatives_dir + file, "r", encoding="utf-8")
    csv_reader = csv.reader(f, delimiter=",")
    criteria_name = ""
    for row_index, row in enumerate(csv_reader):
        if row_index == 0:
            for alt in row[1:]:
                if alt not in alternatives_names:
                    alternatives_names.append(alt)

            criteria_name = row[0]
            num_alternatives = len(row) - 1
        else:
            if not is_inited:
                alternatives_cofs_tables_list = [np.empty([num_alternatives, num_alternatives], dtype=float) for _ in alternatives_files]
                is_inited = True
            for i in range(len(alternatives_names)):
                alternatives_cofs_tables_list[criteria_names.index(criteria_name)][row_index - 1, i] = float(Fraction(row[i + 1]))

    f.close()

criteria_cofs_normed_table = criteria_cofs_table / criteria_cofs_table.sum(axis=0)
alternatives_cofs_normed_tables_list = [a / a.sum(axis=0) for a in alternatives_cofs_tables_list]


criteria_weights_table = np.empty([len(criteria_names), 1], dtype=float)
alternatives_weights_table = np.empty([len(alternatives_names), len(criteria_names)], dtype=float)

for index in range(len(criteria_names)):
    criteria_weights_table[index, 0] = np.mean(criteria_cofs_normed_table, axis=1)[index]

for alt_i in range(len(alternatives_names)):
    for crit_i in range(len(criteria_names)):
        alternatives_weights_table[alt_i, crit_i] = np.mean(alternatives_cofs_normed_tables_list[crit_i], axis=1)[alt_i]

print("Criteria weights: ")
for i, weight in enumerate(criteria_weights_table):
    print(criteria_names[i] + ": " + str(weight[0]))

print("\nAlternatives weights: ")
print(" | ".join(criteria_names))
for i, row in enumerate(alternatives_weights_table):
    row = np.around(row, decimals=2)
    print(alternatives_names[i] + " | " + " | ".join(str(num) for num in row))

res_weights = alternatives_weights_table.dot(criteria_weights_table)

print("\nResulting weights: ")
for i, row in enumerate(res_weights):
    row = np.around(row, decimals=2)
    print(alternatives_names[i] + ": " + str(row[0]))

print("\nMost optimal alternative: " + alternatives_names[np.argmax(res_weights)])
